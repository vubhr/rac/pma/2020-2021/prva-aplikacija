package com.example.helloworld

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    private lateinit var gumbic: Button
    private lateinit var tekstic: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        gumbic = findViewById(R.id.btnStart)
        tekstic = findViewById(R.id.tekstic)

        gumbic.setOnClickListener{ view: View ->
            tekstic.text = "Pozdrav svijete!"
        }
    }


}